package main

import "fmt"

type QueueNode struct {
	value string
	next  *QueueNode
}

type Queue struct {
	head *QueueNode
	tail *QueueNode
	len  int
}

func initQueue() *Queue {
	return &Queue{}
}

func (queue *Queue) enqueue(value string) {
	newNode := &QueueNode{value: value}
	queue.len++
	if queue.head == nil {
		queue.head = newNode
		queue.tail = newNode
		return
	}
	queue.tail.next = newNode
	queue.tail = newNode
}

func (queue *Queue) dequeue() string {
	if queue.head != nil {
		value := queue.head.value
		queue.head = queue.head.next
		queue.len--
		return value
	} else {
		return "The queue is empty"
	}
}

func (queue *Queue) peek() string {
	return queue.head.value // Needs to be enriched with if else queue is empty
}

func (queue *Queue) isEmpty() bool {
	return queue.len == 0
}

func (queue *Queue) delete() {
	queue.head = nil
	queue.tail = nil
	queue.len = 0
}

func (queue *Queue) PrintQueue() string {
	returnString := ""
	currNode := queue.head
	for currNode.next != nil {
		returnString += currNode.value + "<-"
		currNode = currNode.next
	}
	return returnString + currNode.value // Needs to be reversed. The reversion is done in a separate function,
	// Go doesn't offer built-in reverse function (as much as I know)
}

func main() {
	queue := initQueue()
	queue.enqueue("1")
	queue.enqueue("2")
	queue.enqueue("3")
	queue.enqueue("4")
	queue.enqueue("5")

	fmt.Println(queue.dequeue())
	fmt.Println(queue.dequeue())
	fmt.Println(queue.peek())
	fmt.Println(queue.PrintQueue())
	fmt.Println(queue.isEmpty())

	queue.delete()
	fmt.Println(queue.isEmpty())
}

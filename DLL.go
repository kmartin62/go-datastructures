package main

import (
	"fmt"
	"strconv"
)

type DLLNode struct {
	value string
	next *DLLNode
	prev *DLLNode
}

type DLL struct {
	head *DLLNode
	tail *DLLNode
	len int
}

func initListDLL() *DLL {
	return &DLL{}
}

func (dll *DLL) insertFirstDLL(value string) {
	newNode := &DLLNode{value: value}
	if dll.head == nil {
		dll.head = newNode
		dll.tail = newNode
	} else {
		tempNode := dll.head
		dll.head = newNode
		newNode.next = tempNode
		tempNode.prev = newNode
	}
	dll.len++
}

func (dll *DLL) insertLastDLL(value string) {
	if dll.head == nil {
		dll.insertFirstDLL(value)
		return
	}
	newNode := &DLLNode{value: value}
	tempNode := dll.tail
	tempNode.next = newNode
	newNode.prev = tempNode
	dll.tail = newNode
	dll.len++
}

func (dll *DLL) insert(value string, index int) {
	if dll.head == nil {
		dll.insertFirstDLL(value)
		return
	}
	if dll.len < index {
		fmt.Println("Index out of bounds")
		return
	}
	tempNode := dll.head
	counterIndex := 0
	for counterIndex < index - 1 {
		tempNode = tempNode.next
		counterIndex++
	}
	nextNode := tempNode.next
	newNode := &DLLNode{value: value}
	tempNode.next = newNode
	nextNode.prev = newNode

	newNode.next = nextNode
	newNode.prev = tempNode

	dll.len++

}

func (dll *DLL) getFirstDLL() string {
	if dll.head == nil {
		return "The list is empty"
	}
	return dll.head.value
}

func (dll *DLL) getLastDLL() string {
	if dll.head == nil {
		return "The list is empty"
	}
	return dll.tail.value
}

func (dll *DLL) search(value string) string {
	if dll.head == nil {
		return "The list is empty"
	}
	var localIndex = 0 // Same as localIndex := 0
	var headNode = dll.head
	for headNode != nil {
		if headNode.value == value {
			return "The value " + value + " was found on position " + strconv.Itoa(localIndex)
		}
		localIndex++
		headNode = headNode.next
	}
	return "Not found"
}

func (dll *DLL) deleteNode(index int) {
	if dll.head == nil || dll.len < index {
		fmt.Println("The list is empty or index is out of bounds")
		return
	}
	localIndex := 0
	tempNode := dll.head
	for localIndex < index - 1 {
		tempNode = tempNode.next
		localIndex++
	}
	nextNode := tempNode.next.next
	tempNode.next = nextNode
	nextNode.prev = tempNode

	dll.len--
}

func (dll *DLL) deleteList() {
	dll.head = nil
	dll.tail = nil

	dll.len = 0
}

func (dll *DLL) Print() string {
	if dll.len == 0 {
		return "The list is empty"
	}
	currNode := dll.head
	returnString := ""
	for currNode.next != nil {
		returnString += currNode.value + "<->"
		currNode = currNode.next
	}
	return returnString + currNode.value
}

func main() {
	dll := initListDLL()
	dll.insertFirstDLL("1")
	dll.insertFirstDLL("2")
	dll.insertFirstDLL("3")
	dll.insertFirstDLL("4")
	dll.insertLastDLL("6")
	dll.insertLastDLL("7")
	dll.insertLastDLL("8")
	dll.insert("10", 4)
	dll.deleteNode(2)
	dll.deleteList()

	fmt.Println(dll.getFirstDLL())
	fmt.Println(dll.getLastDLL())
	fmt.Println(dll.search("10"))
	fmt.Println(dll.Print())
}
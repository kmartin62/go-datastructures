package main

import "fmt"

type StackNode struct {
	value string
	next  *StackNode
}

type Stack struct {
	head *StackNode
	len  int
}

func initStack() *Stack {
	return &Stack{}
}

func (stack *Stack) isEmpty() bool {
	return stack.len == 0
}

func (stack *Stack) push(value string) {
	newNode := &StackNode{value: value}
	newNode.next = stack.head
	stack.head = newNode
	stack.len++
}

func (stack *Stack) pop() string {
	returnNode := stack.head
	stack.head = returnNode.next
	stack.len--
	return returnNode.value
	// Enrich later with if stack.len == 0, too lazy now
}

func (stack *Stack) peek() string {
	return stack.head.value
	// Enrich later with if stack.len == 0, too lazy now
}

func (stack *Stack) delete() {
	stack.head = nil
	stack.len = 0
}

func (stack *Stack) Print() string {
	if stack.len == 0 {
		return "The stack is empty"
	}
	tempNode := stack.head
	for tempNode != nil {
		fmt.Println(tempNode.value)
		tempNode = tempNode.next
	}
	return ""
}

func main() {
	stack := initStack()
	stack.push("2")
	stack.push("3")
	stack.push("4")
	fmt.Println(stack.isEmpty())
	fmt.Println(stack.pop())
	fmt.Println(stack.peek())
	stack.delete()
	fmt.Println("==============")
	fmt.Println(stack.Print())
}

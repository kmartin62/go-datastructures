package main

import (
	"fmt"
	"strconv"
)


type Node struct {
	next *Node
	value string
}

type SLL struct {
	len int
	head *Node
	tail *Node
}

func initList() *SLL{
	return &SLL{}
}

func (list *SLL) insertFirst(value string) {
	newNode := &Node{value: value}
	if list.head == nil {
		list.head = newNode
		list.tail = newNode
	} else {
		newNode.next = list.head
		list.head = newNode
	}
	list.len++
	return
}

func (list *SLL) insertLast(value string) {
	newNode := &Node{value: value, next: nil}
	if list.head == nil {
		list.insertFirst(value)
	} else {
		list.tail.next = newNode
		list.tail = newNode
	}
	list.len++
	return
}

func (list *SLL) insert(value string, location int) {
	newNode := &Node{value: value}
	if list.head == nil || location == 0 {
		list.insertFirst(value)
	} else if location == 1 {
		list.insertLast(value)
	} else {
		tempNode := list.head
		index := 0
		for index < location - 1 {
			tempNode = tempNode.next
			index++
		}
		newNode.next = tempNode.next
		tempNode.next = newNode
	}
	list.len++
}

func (list *SLL) Print() string {
	currNode := list.head
	if list.len == 0 {
		return "The list is empty"
	} else if list.len == 1 {
		return list.head.value
	}
	var returnString = ""
	for currNode.next != nil {
		returnString += currNode.value + "->"
		currNode = currNode.next
	}
	return returnString + currNode.value

}

func (list *SLL) getFirst() string {
	if list.head == nil {
		return "The list is empty"
	}
	return list.head.value
}

func (list *SLL) getLast() string {
	if list.head == nil {
		return "The list is empty"
	}
	return list.tail.value
}

func (list *SLL) search(value string) string {
	if list.len == 0 {
		return "The list is empty"
	}
	tempNode := list.head
	index := 0
	for tempNode != nil {
		if tempNode.value == value {
			return "Value " + value + " found at index: " + strconv.Itoa(index)
		}
		tempNode = tempNode.next
		index++
	}
	return "Not found"
}

func (list *SLL) deleteNode(index int) {
	if list.len == 0 || list.len < index{
		fmt.Println("The list is empty or the index is out of bounds")
		return
	}
	localIndex := 0
	tempNode := list.head
	for localIndex < index - 1 {
		tempNode = tempNode.next
		localIndex++
	}
	tempNode.next = tempNode.next.next
	list.len--
}

func (list *SLL) deleteList(){
	list.head = nil
	list.tail = nil
	list.len = 0
}

func main() {
	sll := initList()
	sll.insertFirst("1")
	sll.insertFirst("2")
	sll.insertLast("3")
	sll.insertLast("4")
	sll.insertLast("5")
	sll.insertFirst("6")
	sll.insert("7", 3)
	sll.insert("8", 0)
	sll.insert("9", 1)
	fmt.Println(sll.Print())
	fmt.Println(sll.getFirst())
	fmt.Println(sll.getLast())
	fmt.Println(sll.search("9"))

	sll.deleteNode(2)
	sll.deleteNode(4)
	fmt.Println(sll.Print())
	sll.deleteList()
	fmt.Println(sll.Print())
}

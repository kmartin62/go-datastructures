package main

import "fmt"

type BTQueueNode struct {
	data *BinaryTree
	next *BTQueueNode
}

type BTQueue struct {
	head *BTQueueNode
	tail *BTQueueNode
	len  int
}

func initBTQueue() *BTQueue {
	return &BTQueue{}
}

func (queue *BTQueue) enqueue(value *BinaryTree) {
	node := &BTQueueNode{data: value}
	queue.len++
	if queue.head == nil {
		queue.head = node
		queue.tail = node
		return
	}
	queue.tail.next = node
	queue.tail = node
}

func (queue *BTQueue) dequeue() *BinaryTree {
	if queue.head == nil {
		return nil
	}
	queue.len--
	value := queue.head.data
	queue.head = queue.head.next
	return value
}

func (queue *BTQueue) isEmpty() bool {
	return queue.len == 0
}

type BinaryTree struct {
	data       string
	leftChild  *BinaryTree
	rightChild *BinaryTree
}

func initBinaryTree(data string) *BinaryTree {
	return &BinaryTree{data: data}
}

func preOrderTraversal(rootNode *BinaryTree) {
	if rootNode == nil {
		return
	}
	fmt.Println(rootNode.data)
	preOrderTraversal(rootNode.leftChild)
	preOrderTraversal(rootNode.rightChild)
}

func inOrderTraversal(rootNode *BinaryTree) {
	if rootNode == nil {
		return
	}
	inOrderTraversal(rootNode.leftChild)
	fmt.Println(rootNode.data)
	inOrderTraversal(rootNode.rightChild)
}

func postOrderTraversal(rootNode *BinaryTree) {
	if rootNode == nil {
		return
	}
	inOrderTraversal(rootNode.leftChild)
	inOrderTraversal(rootNode.rightChild)
	fmt.Println(rootNode.data)
}

func levelOrderTraversal(rootNode *BinaryTree) {
	if rootNode == nil {
		return
	}
	customQueue := initBTQueue()
	customQueue.enqueue(rootNode)
	for !customQueue.isEmpty() {
		root := customQueue.dequeue()
		fmt.Println(root.data)
		if root.leftChild != nil {
			customQueue.enqueue(root.leftChild)
		}
		if root.rightChild != nil {
			customQueue.enqueue(root.rightChild)
		}
	}
}

func searchBinaryTree(rootNode *BinaryTree, nodeValue string) string {
	if rootNode == nil {
		return "The binary tree is empty"
	}
	customQueue := initBTQueue()
	customQueue.enqueue(rootNode)
	for !customQueue.isEmpty() {
		root := customQueue.dequeue()
		if root.data == nodeValue {
			return root.data
		}

		if root.leftChild != nil {
			customQueue.enqueue(root.leftChild)
		}
		if root.rightChild != nil {
			customQueue.enqueue(root.rightChild)
		}
	}
	return "Not found"
}

func insertNodeInBinaryTree(rootNode *BinaryTree, newNode *BinaryTree) string {
	if rootNode == nil {
		return "The tree is empty"
	}
	customQueue := initBTQueue()
	customQueue.enqueue(rootNode)
	for !customQueue.isEmpty() {
		root := customQueue.dequeue()
		if root.leftChild == nil {
			root.leftChild = newNode
			return "Inserted in left child"
		} else {
			customQueue.enqueue(root.leftChild)
		}

		if root.rightChild == nil {
			root.rightChild = newNode
			return "Inserted in left child"
		} else {
			customQueue.enqueue(root.rightChild)
		}
	}
	return "The insertion failed"
}

func getDeepestNode(rootNode *BinaryTree) *BinaryTree {
	if rootNode == nil {
		return nil
	}
	customQueue := initBTQueue()
	customQueue.enqueue(rootNode)
	root := initBinaryTree("")
	for !customQueue.isEmpty() {
		root = customQueue.dequeue()
		if root.leftChild != nil {
			customQueue.enqueue(root.leftChild)
		}
		if root.rightChild != nil {
			customQueue.enqueue(root.rightChild)
		}
	}
	if root == nil {
		return nil
	} else {
		return root
	}
}

func deleteDeepestNode(rootNode *BinaryTree, deepestNode *BinaryTree) {
	if rootNode == nil {
		return
	}
	customQueue := initBTQueue()
	customQueue.enqueue(rootNode)
	for !customQueue.isEmpty() {
		root := customQueue.dequeue()
		if root.leftChild != nil {
			if root.leftChild == deepestNode {
				root.leftChild = nil
				return
			} else {
				customQueue.enqueue(root.leftChild)
			}
		}
		if root.rightChild != nil {
			if root.rightChild == deepestNode {
				root.rightChild = nil
				return
			} else {
				customQueue.enqueue(root.rightChild)
			}
		}
	}
}

func deleteFromBinaryTree(rootNode *BinaryTree, node *BinaryTree) {
	if rootNode == nil {
		return
	}
	customQueue := initBTQueue()
	customQueue.enqueue(rootNode)
	root := initBinaryTree("")
	for !customQueue.isEmpty() {
		root = customQueue.dequeue()
		if root == node {
			deepestNode := getDeepestNode(rootNode)
			root.data = deepestNode.data
			deleteDeepestNode(rootNode, deepestNode)
			return
		}
		if root.leftChild != nil {
			customQueue.enqueue(root.leftChild)
		}
		if root.rightChild != nil {
			customQueue.enqueue(root.rightChild)
		}
	}
}

func deleteBinaryTree(rootNode *BinaryTree) {
	rootNode.data = ""
	rootNode.leftChild = nil
	rootNode.rightChild = nil
}

func main() {
	rootNode := initBinaryTree("Drinks")
	leftChild := initBinaryTree("Hot")
	rightChild := initBinaryTree("Cold")
	rootNode.leftChild = leftChild
	rootNode.rightChild = rightChild

	//preOrderTraversal(rootNode)
	//inOrderTraversal(rootNode)
	//postOrderTraversal(rootNode)
	//levelOrderTraversal(rootNode)
	//fmt.Println(searchBinaryTree(rootNode, "Hott"))
	//fmt.Println(insertNodeInBinaryTree(rootNode, initBinaryTree("Tea")))
	//fmt.Println(getDeepestNode(rootNode).data)
	//deleteDeepestNode(rootNode, getDeepestNode(rootNode))
	//deleteFromBinaryTree(rootNode, leftChild)
	deleteBinaryTree(rootNode)
	levelOrderTraversal(rootNode)

}

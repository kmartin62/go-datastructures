package main

import (
	"fmt"
	"strings"
)

type Tree struct {
	data     string
	children []*Tree
}

func initTree(data string) *Tree {
	return &Tree{data: data}
}

func (tree *Tree) printTree(level int) string {
	ret := strings.Repeat("-", level) + tree.data + "\n"
	for i := 0; i < len(tree.children); i++ {
		ret += tree.children[i].printTree(level + 1)
	}

	return ret
}

func (tree *Tree) addChild(treeNode *Tree) {
	tree.children = append(tree.children, treeNode)
}

func main() {
	drinks := initTree("Drinks")
	cold := initTree("Cold")
	hot := initTree("Hot")
	drinks.addChild(cold)
	drinks.addChild(hot)
	tea := initTree("Tea")
	coffee := initTree("Coffee")
	cola := initTree("Cola")
	fanta := initTree("Fanta")
	cold.addChild(cola)
	cold.addChild(fanta)
	hot.addChild(tea)
	hot.addChild(coffee)

	fmt.Println(drinks.printTree(0))
}
